<?php
/**
 * Plugin Name: Remove tax amount from emails.
 * Plugin Author: eLightUp
 * Plugin URI: https://elightup.com
 */

// Remove the tax amount in total row in the email.
add_filter( 'woocommerce_get_formatted_order_total', function ( $formatted_total, $obj, $tax_display, $display_refunded ) {
	$formatted_total = wc_price( $obj->get_total(), array( 'currency' => $obj->get_currency() ) );
	$order_total     = $obj->get_total();
	$total_refunded  = $obj->get_total_refunded();
	$tax_string      = '';

	// Tax for inclusive prices.
	if ( wc_tax_enabled() && 'incl' === $tax_display ) {
		$tax_string_array = array();
		$tax_totals       = $obj->get_tax_totals();

		if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) {
			foreach ( $tax_totals as $code => $tax ) {
				$tax_amount         = ( $total_refunded && $display_refunded ) ? wc_price( WC_Tax::round( $tax->amount - $obj->get_total_tax_refunded_by_rate_id( $tax->rate_id ) ), array( 'currency' => $obj->get_currency() ) ) : $tax->formatted_amount;
				$tax_string_array[] = sprintf( '%s %s', $tax_amount, $tax->label );
			}
		} elseif ( ! empty( $tax_totals ) ) {
			$tax_amount         = ( $total_refunded && $display_refunded ) ? $obj->get_total_tax() - $obj->get_total_tax_refunded() : $obj->get_total_tax();
			$tax_string_array[] = sprintf( '%s %s', wc_price( $tax_amount, array( 'currency' => $obj->get_currency() ) ), WC()->countries->tax_or_vat() );
		}

		if ( ! empty( $tax_string_array ) ) {
			/* translators: %s: taxes */
			$tax_string = ' <small class="includes_tax">(includes tax)</small>';
		}
	}

	if ( $total_refunded && $display_refunded ) {
		$formatted_total = '<del>' . strip_tags( $formatted_total ) . '</del> <ins>' . wc_price( $order_total - $total_refunded, array( 'currency' => $obj->get_currency() ) ) . $tax_string . '</ins>';
	} else {
		$formatted_total .= $tax_string;
	}

	return $formatted_total;
}, 10, 4 );
